import boto3
import os

def upload_files(path):
    """
    :param path: the path of the folder containing the files
    recommend: all upload files should be in the same folder to make it easier to upload
    """
    session = boto3.Session(
        aws_access_key_id=os.environ.get('AWS_ACCESS_KEY_ID'),
        aws_secret_access_key=os.environ.get('AWS_SECRET_ACCESS_KEY'),
        region_name='ap-southeast-1'
    )
    list_file = os.listdir(path)
    list_file = [file for file in list_file if file.endswith(".csv")]
    s3 = session.resource('s3')
    for file in list_file:
        file_name = os.path.join(path, file)
        key_name = os.path.basename(file_name)
        s3.meta.client.upload_file(Filename=file_name, Bucket='s3redshifttraining', Key=("ThuanDQ" + "/" + key_name))
if __name__ == "__main__":
    upload_files("/Users/daoquangthuan/Documents/redshift/data/")

